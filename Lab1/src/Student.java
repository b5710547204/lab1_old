
//TODO Write class Javadoc
/**
 * A simple model for a student with a name.
 * 
 * @author Patinya.Y
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Person.Id is the id of the new Person.
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * Initialize a new Person object.
	 * @param name is the name of the new Person
	 */
	public boolean equals(Object other) {
		if(other.getClass().equals(Student.class)){
			Student newOther = (Student)other;
			return this.id == newOther.id;
		}
		else 
			return super.equals(other);
			
	}
}
